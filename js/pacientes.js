$(document).ready(function () {
    $.ajax({
      type: "POST",
      url: "listadoPacientes.php",
      success: (response) => {
        console.log(response);
        var pacientes = JSON.parse(response);
        var resultsHTML = '';

        function imprimirPaciente(clave, pacientes) {
          resultsHTML += '<hr /><div class="fila_paciente"><div class="row">\
            <div class="col-sm-4">\
              <h4 style="margin-bottom: 0">Nombre:</h4>\
              <p>' + pacientes[clave].nombres + ' ' + pacientes[clave].apellidos + '</p></div>\
              <div class="col-sm-4">\
              <h4 style="margin-bottom: 0">Cédula:</h4>\
              <p>' + pacientes[clave].documento + '</p></div>\
              <div class="col-sm-4">\
              <button type="button" class="btn btn-green" data-toggle="modal" data-target="#myModal" onclick="mostrarInfoPaciente(' + pacientes[clave].documento + ');" style="margin-top:15px" >Ver\
              </button></div></div></div>'
        }

        for (var clave in pacientes) {
          imprimirPaciente(clave, pacientes);
        }
        results.innerHTML = resultsHTML;

        $("#boton_buscar").click(function () {
          resultsHTML = '';
          for (var clave in pacientes) {
            var nombre_completo = pacientes[clave].nombres + ' ' + pacientes[clave].apellidos;
            nombre_completo = nombre_completo.toLowerCase();
            var texto_barra = $('input[id=barra_busqueda]').val().toLowerCase();
            if (pacientes[clave].documento == texto_barra || nombre_completo.indexOf(texto_barra) > -1) {
              imprimirPaciente(clave, pacientes);
            }
          }
          results.innerHTML = resultsHTML;
        });
      }
    })
  });

  function mostrarInfoPaciente(documento) {
    $.ajax({
      type: "POST",
      url: "listadoPacientes.php",
      success: (response) => {

        var pacientes = JSON.parse(response);
        for (var clave in pacientes) {
          if (pacientes[clave].documento == documento) {
            var tipo_doc = pacientes[clave].tipo_documento;
            if (tipo_doc == "cédula de ciudadanía") {
              $('select[id=Tipo_Documento]')[0].options.selectedIndex = 0;
            }else 
            if (tipo_doc == "tarjeta de identidad") {
              $('select[id=Tipo_Documento]')[0].options.selectedIndex = 1;
            } else {
              $('select[id=Tipo_Documento]')[0].options.selectedIndex = 2;
            }

            $('input[id=cedula]').val(pacientes[clave].documento);
            $('input[id=nombres]').val(pacientes[clave].nombres);
            $('input[id=apellidos]').val(pacientes[clave].apellidos);
            $('input[id=direccion]').val(pacientes[clave].direccion);
            $('input[id=email]').val(pacientes[clave].email);
            $('input[id=edad]').val(pacientes[clave].edad);
            $('input[id=genero]').val(pacientes[clave].genero);
            $('input[id=telefono]').val(pacientes[clave].telefono);
            // $('input[id=eps]').val(pacientes[clave].documento);
          }
        }
      }
    })
  }

  // $("#myForm").submit(function (e) {
  //   e.preventDefault();
  // });

  $('#agregar-paciente').click(() => {
    $('#myForm').ajaxSubmit({
      url: "loadPaciente.php", success: function (result) {
        alert('Registro exitoso');
        window.location.replace("./pacientes.html");
      }
    })
  })