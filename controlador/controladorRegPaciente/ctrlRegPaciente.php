<?php

include_once 'db/daoPaciente.php';
include_once 'modelo/Persona.php';
include_once 'modelo/Paciente.php';

class ctrlRegPaciente
{

    private $DAOPaciente;
    private $paciente;
    


    public function ctrlRegPaciente()
    {
        $this->paciente=new Paciente();
        $this->DAOPaciente=new DAOPaciente($this->paciente);
        $this->DAOPaciente->openConexion();
    } 

    public function cargarPaciente($documento,$tipo_documento,$nombres,$apellidos,$direccion,$correo,$edad,$genero,$telefono,$eps)
    {
      
         //$this->paciente=new Paciente();
         $this->paciente->setDocumento($documento);
         $this->paciente->setTipoDocumento($tipo_documento);
         $this->paciente->setNombres($nombres);
         $this->paciente->setApellidos($apellidos);
         $this->paciente->setDireccion($direccion);
         $this->paciente->setCorreo($correo);
         $this->paciente->setEdad($edad);
         $this->paciente->setGenero($genero);
         $this->paciente->setTelefono($telefono);
         $this->paciente->setEps($eps);
         $this->DAOPaciente->setPaciente($this->paciente);



    }

    public function registrarPaciente()
    {
        $response=$this->DAOPaciente->insertarPaciente();
        return $response;
    }


    public function listarPacientes()
    {
        $response=$this->DAOPaciente->listarPacientes();
        return $response;
    }


    public function buscarPaciente($documento)
    {
        $paciente=new Paciente();
        $paciente->setDocumento($documento);
        $this->DAOPaciente->setPaciente($paciente); 
        $auxPaciente=$this->DAOPaciente->buscarPaciente();
        $response=$this->DAOPaciente->getHeader();
        if($response['operacion']=="Exitosa")
        {
           return $auxPaciente;
        }

        return null;
    }



   






}










?>