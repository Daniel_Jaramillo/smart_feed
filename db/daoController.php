<?php

include_once'dbconexion.php';


class DAOController
{
    
    protected $conexion;
    protected $query;
    protected $header=Array();  
    protected $contenido=Array(); 

    public function DAOController()
    {
       
        $this->query="";
    }

    public function openConexion()
    {
        $this->conexion=new dbconexion();       
        $this->conexion->conectar();;
        
    }

    public function closeConexion()
    {
       
            $this->conexion->desconectar();
       
    }


   public function setConexion($conexion)
   {
       $this->conexion=$conexion;
   }

    public function getConexion()
    {
        return $this->conexion;
    }

    public function setQuery($query)
    {
      $this->query=$query;
    }

    public function getQuery()
    {
        return $query;
    }



    public function cargarDatos($tipoOperacion)
    {
      
             $this->header=Array(
            'operacion'=>'Exitosa',
            'tipo_Operacion'=>$tipoOperacion);
    }

    public function notificarError($tipoOperacion)
    {
       
            $this->header=Array(
            'operacion'=>'Fallo',
            'tipo_Operacion'=>$tipoOperacion);     
      
        
    }


    public function confirmarTransaccion($operacion)
    {
        
        $response=$this->conexion->consulta($this->query);
      
      
        if($operacion=="Insertar")
       {
            if($response)
            {
                $this->cargarDatos($operacion);
            }

            else{
                
                $this->notificarError($operacion);

            }
       }

       else if($operacion=="Listar")
       {
           $numeroFila=mysqli_num_rows($response); 
           if($numeroFila!=0)
           {
                 $this->cargarDatos($operacion);
                 if($operacion=="Listar")
                 {
                  
                   $this->contenido=$response;
                  
                 }
    
           }
           else
           {
               $this->notificarError($operacion);
               $this->contenido=$response;
           }
      

       }
	   
	   else if($operacion=="Actualizar")
	   {
	   
	      
		   if($response)
            {
                $this->cargarDatos($operacion);
            }

            else{
                
                $this->notificarError($operacion);

            }
		  
		  
		  
		  
	   }




   
        

       
       

       
       



    }


    public function setHeader($header)
    {
       $this->header=$header;
    }

    public function getHeader()
    {
        return $this->header;
    }

  




}


?>