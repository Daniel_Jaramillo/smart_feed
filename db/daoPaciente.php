<?php

include_once'modelo/paciente.php';
include_once'daoPersona.php';

class DAOPaciente extends DAOController{



    private $paciente;
    private $templatePacientes=Array();


    public function DAOPaciente($paciente)
    {
       $this->paciente=new $paciente;
    }

    public function insertarPaciente()
    {
        
        $paciente=new Paciente();
        $DAOPersona=new DAOPersona($paciente);
        $DAOPersona->openConexion();
        $DAOPersona->setPersona($this->paciente);
        
        $respuesta=$DAOPersona->insertarPersona();
        if($respuesta['operacion']=='Exitosa')
        {
            $DAOPersona->closeConexion();
            $this->query='insert into paciente (eps,documento) values
            ("'.$this->paciente->getEps().'",
            "'.$this->paciente->getDocumento().'"
            ) ';

         $this->confirmarTransaccion("Insertar");
         $this->buscarPaciente();
         return $this->header;


        }
        
        
       
        
    }



    public function buscarPaciente()
    {
       $this->query='select * from persona inner join paciente on persona.documento=paciente.documento
       where persona.documento="'.$this->paciente->getDocumento().'"';
       $this->confirmarTransaccion("Listar");


       while ($reg=mysqli_fetch_row($this->contenido))
       {  
         
         $this->paciente->setTipoDocumento($reg['0']);
         $this->paciente->setNombres($reg['7']);
         $this->paciente->setApellidos($reg['2']);
         $this->paciente->setEdad($reg['4']);
         $this->paciente->setCorreo($reg['5']);  
                           
     
           
       }

       return $this->paciente;


    }

    
 
   public function listarPacientes()
    {
        $this->query='select * from persona inner join paciente on persona.documento=paciente.documento';
        $this->confirmarTransaccion("Listar");

        while ($reg=mysqli_fetch_row($this->contenido))
        {  
          $this->templatePacientes[]=Array(
              'documento'=> ''.$reg['0'].'',
              'tipo_documento'=> $reg['8'],
              'nombres'=> $reg['6'],
              'apellidos'=>$reg['1'],
              'edad'=>$reg['3'],
              'genero'=>$reg['5'],
              'email'=>$reg['4'],
              'telefono'=>$reg['7'],
              'direccion'=>$reg['2']              
              );

            
        }

        return $this->templatePacientes;
    


    }


    public function setPaciente($paciente)
    {
        $this->paciente=$paciente;
    }

    public function getPaciente()
    {
       return $this->paciente;
    }







}

?>