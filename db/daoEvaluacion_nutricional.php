<?php

class DAOEvaluacion_nutricional extends DAOController
{

   private $evaluacion_nutricional;
   private $templateEvaluacion=Array();

   public function DAOEvaluacion_nutricional($evaluacion_nutricional)
   {
       $this->DAOEvaluacion_nutricional=$evaluacion_nutricional;
   }

   public function insertarEvaluacion()
   {
       $this->query='insert into evaluacion_nutricional 
       (perimetro_brazo_relajdo,
       perimetro_brazo_tenso,
       perimetro_cintura,
       perimetro_muslo,
       peso,
       pliegue_abdominla,
       pliegue_bicipital,
       pliegue_muslo,
       pliegue_tricipital,
       talla)
       values ('.$this->evaluacion_nutricional->getPerimetro_brazo_relajado().',
       '.$this->evaluacion_nutricional->getPerimetro_brazo_tenso().',
       '.$this->evaluacion_nutricional->getPerimetro_cintura().',
       '.$this->evaluacion_nutricional->getPerimetro_muslo().',
       '.$this->evaluacion_nutricional->getPeso().',
       '.$this->evaluacion_nutricional->getPliegue_abdominal().',
       '.$this->evaluacion_nutricional->getPliegue_bicipital().',
       '.$this->evaluacion_nutricional->getPliegue_bicipital().',
       '.$this->evaluacion_nutricional->getPliegue_tricipital().',
       '.$this->evaluacion_nutricional->getTalla().')';
	   
	   echo $this->evaluacion_nutricional->getPliegue_muslo();
	   
       $this->confirmarTransaccion("Insertar");
       $this->buscarEvaluacion();
   }
   
   
   public function buscarEvaluacion()
   {  
            
    $this->query='select * from evaluacion_nutricional where
    perimetro_brazo_relajdo='.$this->evaluacion_nutricional->getPerimetro_brazo_relajado().' and
    perimetro_brazo_tenso='.$this->evaluacion_nutricional->getPerimetro_brazo_tenso().' and
    perimetro_cintura='.$this->evaluacion_nutricional->getPerimetro_cintura().' and
    perimetro_muslo='.$this->evaluacion_nutricional->getPerimetro_muslo().' and
    peso='.$this->evaluacion_nutricional->getPeso().' and
    pliegue_abdominla='.$this->evaluacion_nutricional->getPliegue_abdominal().' and
    pliegue_bicipital='.$this->evaluacion_nutricional->getPliegue_bicipital().' and
    pliegue_muslo='.$this->evaluacion_nutricional->getPliegue_bicipital().' and
    pliegue_tricipital='.$this->evaluacion_nutricional->getPliegue_tricipital().' and
    talla='.$this->evaluacion_nutricional->getTalla().'';  
   
   $this->confirmarTransaccion("Listar");

   while ($reg=mysqli_fetch_row($this->contenido))
   {
       $this->evaluacion_nutricional->setId($reg['0']);
   }
          
   return $this->evaluacion_nutricional;
   
   }

   public function listarEvaluaciones()
   {
       $this->query='select * from evaluacion_nutricional';
   }

   public function deleteEvalaucion($codigoEvaluacion)
   {
       $this->query='delete from evaluacion_nutricional where evaluacion_nutricional.id='.$codigoEvaluacion.'';
   }

   public function buscarCodigoEvalaucion()
   {
       $this->query='select * from evaluacion_nutricional where
       perimetro_brazor_relajado='.$this->evaluacion_nutricional->getPerimetro_brazor_relajado().'and
       perimetro_brazo_tenso='.$this->evaluacion_nutricional->getPerimetro_brazo_tenso().' and
       perimetro_cintura='.$this->evaluacion_nutricional->getPerimetro_cintura().' and
       perimetro_muslo='.$this->evaluacion_nutricional->getPerimetro_muslo().' and
       peso='.$this->evaluacion_nutricional->getPeso().' and
       pliegue_abdominal='.$this->evaluacion_nutricional->getPliegue_abdominal().' and,
       pliegue_bicipital='.$this->evaluacion_nutricional->getPliegue_bicipital().' and,
       pliegue_tricipital='.$this->evaluacion_nutricional->getPliegue_tricipital().' and,
       talle='.$this->evaluacion_nutricional->getTalla().'';
       return $codigoEvaluacion;

   }


   public function updateEvaluacion($codigoEvaluacion)
   {
       $this->query='update evaluacion_nutricional set perimetro_brazor_relajado='.$this->evaluacion_nutricional->getPerimetro_brazor_relajado().'and
       perimetro_brazo_tenso='.$this->evaluacion_nutricional->getPerimetro_brazo_tenso().' and
       perimetro_cintura='.$this->evaluacion_nutricional->getPerimetro_cintura().' and
       perimetro_muslo='.$this->evaluacion_nutricional->getPerimetro_muslo().' and
       peso='.$this->evaluacion_nutricional->getPeso().' and
       pliegue_abdominal='.$this->evaluacion_nutricional->getPliegue_abdominal().' and,
       pliegue_bicipital='.$this->evaluacion_nutricional->getPliegue_bicipital().' and,
       pliegue_tricipital='.$this->evaluacion_nutricional->getPliegue_tricipital().' and,
       talle='.$this->evaluacion_nutricional->getTalla().'
       where evalucion_nutricional='.$codigoEvaluacion.'';
   }
   
   public function setEvaluacion_nutricional($evaluacion)
   {
       $this->evaluacion_nutricional=$evaluacion;
	   
   }
   
   public function getEvaluacion_nutricional()
   {
      return $this->evaluacion_nutricional;
   }

   public function listarEvaluacionNutricional($idConsulta)
   {
      $this->query='select * from evaluacion_nutricional inner join consulta on evaluacion_nutricional.id=consulta.evaluacion
      where consulta.id='.$idConsulta.';';
      $this->confirmarTransaccion("Listar");

    
      while ($reg=mysqli_fetch_row($this->contenido))
        {  
          $this->templateEvaluacion[]=Array(
              'id'=>$reg['0'],
              'perimetro_brazo_relajado'=>$reg['0'],
              'perimetro_brazo_tenso'=>$reg['1'],
              'perimetro_cintura'=>$reg['2'],              
              'perimetro_muslo'=>$reg['3'],
              'peso'=>$reg['4'],
              'pliegue_abdominal'=>$reg['5'],          
              'pliegue_bicipital'=>$reg['6'],
              'pliegue_muslo'=>$reg['7'],
              'pliegue_tricipital'=>$reg['8'],
              'talla'=>$reg['9']
                       
              );

            
        }


        return $this->templateEvaluacion;

   }  

   



}




?>