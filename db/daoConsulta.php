<?php



class DAOConsulta extends DAOController
{
   
    private $consulta;
    private $templateConsulta=Array();

    public function DAOConsulta($consulta)
    {
       $this->consulta=$consulta;
    }

    public function insertarConsulta()
    {
        $this->query='insert into consulta(estado,fecha,observacion,nutricionista,paciente,hora)
        values(
        "'.$this->consulta->getEstado().'",
        "'.$this->consulta->getFecha().'",
        "'.$this->consulta->getObservacion().'",
		"'.$this->consulta->getNutricionista()->getDocumento().'",
        "'.$this->consulta->getPaciente()->getDocumento().'",
        "'.$this->consulta->getHora().'"
        )';
		

        $this->confirmarTransaccion("Insertar");
        

        $this->buscarConsulta();
        return $this->header;
		
		
    }

    public function updateConsulta($codigoConsulta)
    {
        $this->query='update consulta set estado="'.$this->consulta->getEstado().'" and
        fecha="'.$this->consulta->getFecha().'" and
        observacion="'.$this->consulta->getPaciente()->getDocumento().'" and
        paciente="'.$this->consulta->getPaciente()->getDocumento().'" and
        nutricionista="'.$this->consulta->getNutricionista()->getDocumento().'"';



    }

    public function deleteConsulta($codigoConsulta)
    {
        $this->query='delete from consulta where consulta.id='.$codigoConsulta.'';
    }
    public function buscarCodigo()
    {
        $this->query='select * from consulta where consulta.fecha="'.$this->consulta->getFecha().'"
        and consulta.paciente="'.$this->consulta->getPaciente()->getDocumento().'" and
        consulta.nutricionista="'.$this->consulta->getNutricionista().'"';
        return $codigoConsulta;
    }

    public function buscarConsulta()
    {
        $this->query='select * from consulta where consulta.estado="'.$this->consulta->getEstado().'"
        and consulta.fecha="'.$this->consulta->getFecha().'"and
        consulta.nutricionista="'.$this->consulta->getNutricionista()->getDocumento().'" and
        consulta.paciente="'.$this->consulta->getPaciente()->getDocumento().'"';
        $this->confirmarTransaccion("Listar");

        while ($reg=mysqli_fetch_row($this->contenido))
        {
            $this->consulta->setEstado($reg['1']);
            $this->consulta->setFecha($reg['2']);
            $this->consulta->setObservacion($reg['3']);
            $tmpPaciente=$reg['5'];
            $this->consulta->getPaciente()->setDocumento($tmpPaciente);
            $tmpNutricionista=$reg['4'];
            $this->consulta->getNutricionista()->setDocumento($tmpNutricionista);
         


            
        }

        return $this->consulta;




      
    }



    public function bucarConsultaTemplateId($idConsulta)
    {
        //$this->query='select * from consulta where consulta.id='.$idConsulta.'';
        
        /*$this->query='select consulta.id,consulta.ESTADO,consulta.FECHA,consulta.OBSERVACION,persona.documento,persona.NOMBRES,persona.APELLIDOS from consulta
        inner join paciente on consulta.paciente= paciente.documento inner join persona on paciente.documento= paciente.documento
        where consulta.id='.$idConsulta.'';
        */


        $this->query='select * from persona inner join paciente on persona.documento=paciente.documento inner join consulta
        on paciente.documento=consulta.paciente where consulta.id='.$idConsulta.'';
        $this->confirmarTransaccion("Listar");

        while ($reg=mysqli_fetch_row($this->contenido))
        {
             $this->templateConsulta[]=Array(
              'id'=>$reg['12'],
              'estado'=>$reg['13'],
              'fecha'=> $reg['14'],
              'observacion'=>$reg['15'],
              'paciente_documento'=>$reg['0'],
              'paciente_nombres'=>$reg['6'],
              'paciente_apellidos'=>$reg['1']
                           
              );
			
			
			        


            
        }

        return $this->templateConsulta;






    }
	
	
	public function buscarConsultaTemplate()
	{
	  
	  
	  
	  $this->query='select * from consulta where consulta.estado="'.$this->consulta->getEstado().'"
        and consulta.fecha="'.$this->consulta->getFecha().'"and
        consulta.nutricionista="'.$this->consulta->getNutricionista()->getDocumento().'" and
        consulta.paciente="'.$this->consulta->getPaciente()->getDocumento().'"';
        $this->confirmarTransaccion("Listar");

        while ($reg=mysqli_fetch_row($this->contenido))
        {
             $this->templateConsulta[]=Array(
              'id'=>$reg['0'],
              'estado'=>$reg['1'],
              'fecha'=> $reg['2'],
              'observacion'=>$reg['3'],
              'nutricionista'=>$reg['4'],
              'paciente'=>$reg['5'],
              'evaluacion'=>$reg['6']
                           
              );
			
			
			        


            
        }

        return $this->templateConsulta;


	  
	  
	  
	  
	}


  



    public function listarConsultas()
    {
        $this->query='select * from consulta';
        $this->confirmarTransaccion("Listar");


        while ($reg=mysqli_fetch_row($this->contenido))
        {  
          $this->templateConsulta[]=Array(
              'id'=>$reg['0'],
              'estado'=>$reg['1'],
              'fecha'=> $reg['2'],
              'observacion'=>$reg['3'],
              'nutricionista'=>$reg['4'],
              'paciente'=>$reg['5'],
              'evaluacion'=>$reg['6']
                           
              );

            
        }

        return $this->templateConsulta;




        


    }


    public function listarConsultaEstado()
    {
        //$this->query='select * from consulta where consulta.estado="'.$this->consulta->getEstado().'"';
        /*$this->query='select consulta.id,consulta.ESTADO,consulta.FECHA,consulta.OBSERVACION,persona.documento,persona.NOMBRES,persona.APELLIDOS,consulta.evaluacion from consulta
        inner join paciente on consulta.paciente= paciente.documento inner join persona on paciente.documento= paciente.documento
        where consulta.estado="'.$this->consulta->getEstado().'" group by consulta.id;';
        */

        $this->query='select * from persona inner join paciente on persona.documento=paciente.documento inner join consulta
        on paciente.documento=consulta.paciente where consulta.estado="'.$this->consulta->getEstado().'";';
        $this->confirmarTransaccion("Listar");

        while ($reg=mysqli_fetch_row($this->contenido))
        {  
          $this->templateConsulta[]=Array(
              'id'=>$reg['12'],
              'estado'=>$reg['13'],
              'fecha'=> $reg['14'],
              'observacion'=>$reg['15'],
              'paciente_documento'=>$reg['0'],
              'paciente_nombres'=>$reg['6'],
              'paciente_apellidos'=>$reg['1'],
              'evaluacion'=>$reg['18'],
              'correo'=>$reg['4'],
              'telefono'=>$reg['7'],
              'genero'=>$reg['5'],
              'direccion'=>$reg['2'],
              'hora'=>$reg['19']
                           
              );

            
        }

        return $this->templateConsulta;





    }

	
	
	public function recuperarConsulta($idConsulta)
	{
	    $this->query='select * from consulta where id='.$idConsulta.'';
		$this->confirmarTransaccion("Listar");
		
		while ($reg=mysqli_fetch_row($this->contenido))
		{
		     $this->consulta->setIdConsulta($reg['0']);
			 $this->consulta->setEstado($reg['1']);
			 $this->consulta->setFecha($reg['2']);
			 $this->consulta->setObservacion($reg['3']);
			 $nutricionista=new Nutricionista();
			 $nutricionista->setDocumento($reg['4']);
			 $this->consulta->setNutricionista($nutricionista);
			 $paciente=new Paciente();
			 $paciente->setDocumento($paciente);
			 $this->consulta->setPaciente($paciente);
			 
		   
		}
		
		return $this->consulta;
		
		
	
	}
	
	public function agregarEvaluacion()
	{   
	    $this->query='update consulta set evaluacion='.$this->consulta->getEvaluacionNutricional()->getId().'
		where consulta.id='.$this->consulta->getIdConsulta().'';
       
        
		$this->confirmarTransaccion("Actualizar");
		
    }
    

    public function listadoConsultasPaciente()
    {
        
        //$this->query='select * from consulta where consulta.paciente="'.$this->consulta->getPaciente()->getDocumento().'"';
        
        /*$this->query='select consulta.id,consulta.ESTADO,consulta.FECHA,consulta.OBSERVACION,persona.documento,persona.NOMBRES,persona.APELLIDOS from consulta
        inner join paciente on consulta.paciente= paciente.documento inner join persona on paciente.documento= paciente.documento
        where consulta.paciente="'.$this->consulta->getPaciente()->getDocumento().'";';
        */

        $this->query='select * from persona inner join paciente on persona.documento=paciente.documento inner join consulta
        on paciente.documento=consulta.paciente where paciente.documento="'.$this->consulta->getPaciente()->getDocumento().'";';
        

        $this->confirmarTransaccion("Listar");

        while ($reg=mysqli_fetch_row($this->contenido))
        {
            $this->templateConsulta[]=Array(
                'id'=>$reg['12'],
                'estado'=>$reg['13'],
                'fecha'=> $reg['14'],
                'observacion'=>$reg['15'],
                'paciente_documento'=>$reg['0'],
                'paciente_nombres'=>$reg['6'],
                'paciente_apellidos'=>$reg['1']
                           
                );
        }

        return $this->templateConsulta;
    
    }


    public function actualizarEstadoConsulta($idConsulta,$estado)
    {
        $this->query='update consulta set estado="'.$estado.'"
        where consulta.id='.$idConsulta.'';

        $this->confirmarTransaccion("Actualizar");
    }
	
	


    public function setConsulta($consulta)
    {
        $this->consulta=$consulta;
    }


    public function getConsulta()
    {
        return $this->consulta;
    }






  

}


?>