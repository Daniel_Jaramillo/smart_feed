<?php


class Persona {

protected $documento;
protected $tipo_documento;
protected $nombres;
protected $apellidos;
protected $direccion;
protected $correo;
protected $edad;
protected $genero;
protected $telefono;

public function Persona($documento,$tipo_documento,$nombres,$apellidos,$direccion,$correo,$edad,$genero,$telefono)
{
    $this->documento=$documento;
    $this->tipo_Documento=$tipo_documento;
    $this->nombres=$nombres;
    $this->apellidos=$apellidos;
    $this->correo=$correo;
    $this->edad=$edad;
    $this->genero=$genero;
    $this->telefono=$telefono;

}


public function setDocumento($documento)
{
    $this->documento=$documento;
}
public function getDocumento()
{
  return $this->documento;
}



public function setTipoDocumento($tipo_documento)
{
    $this->tipo_documento=$tipo_documento;
}


public function getTipoDocumento()
{
  return $this->tipo_documento;
}



public function setNombres($nombres)
{
    $this->nombres=$nombres;
}


public function getNombres()
{
  return $this->nombres;
}


public function setApellidos($apellidos)
{
    $this->apellidos=$apellidos;
}


public function getApellidos()
{
  return $this->apellidos;
}


public function setCorreo($correo)
{
   $this->correo=$correo;

}

public function getCorreo()
{
    return $this->correo;
}

public function setEdad($edad)
{
   $this->edad=$edad;
}

public function getEdad()
{
    return $this->edad;
}


public function  setGenero($genero)
{
    $this->genero=$genero;
}

public function getGenero()
{
    return $this->genero;
}

public function setDireccion($direccion)
{
  $this->direccion=$direccion;
}

public function getDireccion()
{
    return $this->direccion;
}

public function setTelefono($telefono)
{
    $this->telefono=$telefono;
}

public function getTelefono()
{
    return $this->telefono;
}







}




?>