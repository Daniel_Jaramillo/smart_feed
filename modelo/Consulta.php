<?php

include_once 'Nutricionista.php';
include_once 'Paciente.php';
include_once 'Evaluacion_nutricional.php';


class Consulta
{  private $idConsulta;
   private $nutricionista;
   private $paciente;
   private $fecha;
   private $estado;
   private $observacion;
   private $evaluacion_nutricional;
   private $hora;


   public function Consulta()
   {
     $this->nutricionista=new Nutricionista();
     //$this->paciente=new Paciente();
     $this->evaluacion_nutricional=new Evaluacion_nutricional();
     $this->fecha="";
     $this->estado="Agendado";
     $this->observacion="";
   }


   public function setNutricionista($nutricionista)
   {
       $this->nutricionista=$nutricionista;  
   }

   public function getNutricionista()
   {
       return $this->nutricionista;
   }

   public function setPaciente($paciente)
   {
       $this->paciente=$paciente;
   }

   public function getPaciente()
   {
       return $this->paciente;
   }

   public function setFecha($fecha)
   {
       $this->fecha=$fecha;
   }

   public function getFecha()
   {
       return $this->fecha;
   }

   public function setEstado($estado)
   {
     $this->estado=$estado;
   }

   public function getEstado()
   {
       return $this->estado;
   }

   public function setObservacion($observacion)
   {
     $this->observacion=$observacion;
   }

   public function getObservacion()
   {
       return $this->observacion;
   }
   
   public function setEvaluacionNutricional($evaluacion_nutricional)
   {
         $this->evaluacion_nutricional=$evaluacion_nutricional;
		 
   }
   
   public function getEvaluacionNutricional()
   {
      return $this->evaluacion_nutricional;
   }
   
   public function setIdConsulta($idConsulta)
   {      
      $this->idConsulta=$idConsulta;   
   }
   
   public function getIdConsulta()
   {
      return $this->idConsulta;
   }

   public function setHora($hora)
   {
       $this->hora=$hora;
   }

   public function getHora()
   {
       return $this->hora;
   }



}



?>