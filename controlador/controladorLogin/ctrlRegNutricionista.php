<?php


include_once 'modelo/Persona.php';
include_once 'db/daoNutricionista.php';
include_once 'modelo/Nutricionista.php';
include_once 'modelo/Persona.php';

class CtrlRegNutricionista
{
    private $DAONutricionista;
    private $nutricionista;


    public function CtrlRegNutricionista()
    {
     
        $nutricionista=new Nutricionista();
        $this->DAONutricionista=new DAONutricionista($nutricionista);
        $this->DAONutricionista->openConexion();
        
    }

    public function cargarRegistro($documento,$tipo_documento,$nombres,$apellidos,$direccion,$correo,$edad,$genero,$telefono,$usuario,$password)
    {
       
       
         $this->nutricionista=new Nutricionista();
         $this->nutricionista->setDocumento($documento);
         $this->nutricionista->setTipoDocumento($tipo_documento);
         $this->nutricionista->setNombres($nombres);
         $this->nutricionista->setApellidos($apellidos);
         $this->nutricionista->setDireccion($direccion);
         $this->nutricionista->setCorreo($correo);
         $this->nutricionista->setEdad($edad);
         $this->nutricionista->setGenero($genero);
         $this->nutricionista->setTelefono($telefono);
         $this->nutricionista->setUsuario($usuario);
         $this->nutricionista->setPassword($password);
         $this->DAONutricionista->setNutricionista($this->nutricionista);

    }


    public function registrarNutricionista()
    {
        $response=$this->DAONutricionista->insertarNutricionista();
        return $response;
    }

    public function listarNutricionistas()
    {
        $response=$this->DAONutricionista->listarNutricionistas();
        return $response;
    }
	
	public function buscarNutricionista($documento)
	{   
	    $nutricionista=new Nutricionista();
        $nutricionista->setDocumento($documento);
		$this->DAONutricionista->setNutricionista($nutricionista);
		$auxNutricionista=$this->DAONutricionista->buscarNutricionistaCedula($documento);
				
        $response=$this->DAONutricionista->getHeader();
        
        if($response['operacion']=="Exitosa")
        {
           return $auxNutricionista;
        }

        return null;

	
	}










}







?>