<?php

//include_once'controlador/controladorRegPaciente/ctrlRegPaciente.php';
include_once 'controlador/controladorRegPaciente/ctrlRegPaciente.php';
include_once 'controlador/controladorLogin/ctrlRegNutricionista.php';
include_once 'modelo/Consulta.php';
include_once 'db/daoConsulta.php';
include_once 'db/daoEvaluacion_nutricional.php';


class ctrlRegConsulta
{

    private $DAOConsulta;
    private $DAOPaciente;
    private $consulta;
    private $ctrlPaciente;
    private $DAOevaluacion_nutricional;



    public function ctrlRegConsulta()
    {
        
        $this->consulta=new Consulta();
        $this->DAOConsulta=new DAOConsulta($this->consulta);
        $this->DAOConsulta->openConexion();
        
    } 

    public function agregarPaciente($documento)
    {
          $ctrlPaciente=new ctrlRegPaciente();          
          $response=$ctrlPaciente->buscarPaciente($documento);
          
          if($response!=null)
          {
              $this->consulta->setPaciente($response);
          }

    }

    public function agregarNutricionista($documento)
    {
       $ctrlNutricionista=new ctrlRegNutricionista();
       $response=$ctrlNutricionista->buscarNutricionista($documento);
       
       if($response!=null)
       {
           $this->consulta->setNutricionista($response);
       }


    }

    public function agregarDetalle($fecha,$observacion,$paciente_documento,$nutricionista_documento,$hora)
    {
          $this->consulta->setFecha($fecha);
          $this->consulta->setObservacion($observacion);
          $this->consulta->setEstado("Agendada");
          $this->consulta->setHora($hora);
          $this->agregarPaciente($paciente_documento);
          $this->agregarNutricionista($nutricionista_documento);
    }

    
    
    public function registrarConsulta()
    {
        $response=$this->DAOConsulta->insertarConsulta();
        return $response;
    }

    public function listarConsultas($estado)
    {
       $this->consulta->setEstado($estado);
       $this->DAOConsulta->setConsulta($this->consulta);
       $response=$this->DAOConsulta->listarConsultaEstado();
       return $response;



    }


    public function anexarEvaluacion($peso,$talla,$pliegue_tricipital,$pliegue_bicipital,$pliegue_abdominal,$pliegue_muslo,$perimetro_brazo_tenso,$perimetro_brazo_relajado,$perimetro_muslo,$perimetro_cintura,$idConsulta)
    {
         
        $evaluacion=new Evaluacion_nutricional();
        $evaluacion->setPeso($peso);
        $evaluacion->setTalla($talla);
        $evaluacion->setPliegue_tricipital($pliegue_tricipital);
        $evaluacion->setPliegue_bicipital($pliegue_bicipital);
        $evaluacion->setPliegue_abdominal($pliegue_abdominal);
        $evaluacion->setPliegue_muslo($pliegue_muslo);
        $evaluacion->setPerimetro_brazo_tenso($perimetro_brazo_tenso);
        $evaluacion->setPerimetro_brazo_relajado($perimetro_brazo_relajado);
        $evaluacion->setPerimetro_muslo($perimetro_muslo);
        $evaluacion->setPerimetro_cintura($perimetro_cintura);

                    
        $reponseConsulta=$this->DAOConsulta->recuperarConsulta($idConsulta);
        if($reponseConsulta!=null)
        {   
            $this->consulta=$reponseConsulta;
            $this->consulta->setEvaluacionNutricional($evaluacion);
            $this->DAOevaluacion_nutricional=new DAOEvaluacion_nutricional($evaluacion);
            $this->DAOevaluacion_nutricional->setEvaluacion_nutricional($evaluacion);
            $this->DAOevaluacion_nutricional->openConexion();
            $this->DAOevaluacion_nutricional->insertarEvaluacion();
            $tmp=$this->DAOevaluacion_nutricional->getHeader();
            
            if($tmp['operacion']=="Exitosa")
            {
               
                $evaluacion=$this->DAOevaluacion_nutricional->getEvaluacion_nutricional();
                              
                $this->consulta->setEvaluacionNutricional($evaluacion);
                $this->DAOConsulta->setConsulta($this->consulta);
                $this->DAOConsulta->agregarEvaluacion();
                $answer=$this->DAOConsulta->getHeader();
                return $answer;
            }

           
            
        }     
        
    }


    public function agregarEvaluacionConsulta($idConsulta)
    {
        $reponseConsulta=$this->DAOConsulta->recuperarConsulta($idConsulta);

        if($reponseConsulta!=null)
        {
            $this->consulta=$reponseConsulta;
            
        }

    }


    public function listarConsultaPaciente($documento)
    {
         $paciente=new Paciente();
         $paciente->setDocumento($documento);
         $this->consulta->setPaciente($paciente);
         $this->DAOConsulta->setConsulta($this->consulta);
         $response=$this->DAOConsulta->listadoConsultasPaciente();
         return $response;

    }

    public function visualizarDetalleConsulta($idConsulta)
    {
        
        $reponse1=$this->DAOConsulta->bucarConsultaTemplateId($idConsulta);
        $evaluacion=new Evaluacion_nutricional();
        $DAOEvaluacion=new DAOEvaluacion_nutricional($evaluacion);
        $DAOEvaluacion->openConexion();
        $response2=$DAOEvaluacion->listarEvaluacionNutricional($idConsulta);


        $template[]=Array(
            'infoConsulta'=>$reponse1,
            'infoEvaluacion'=>$response2
        );



        return $template;
    }


    public function modificarEstadoConsulta($idConsulta,$estado)
    {
        
        $this->DAOConsulta->actualizarEstadoConsulta($idConsulta,$estado);
        $response=$this->DAOConsulta->getHeader();
        return $response;
         
    }


    
    



}




?>