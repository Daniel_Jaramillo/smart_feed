<?php

class Evaluacion_nutricional
{


    private $id;
    private $peso;
    private $talla;
    private $pliegue_tricipital;
    private $pliegue_bicipital;
    private $pliegue_abdominal;
    private $pliegue_muslo;
    private $perimetro_brazo_tenso;
    private $perimetro_brazo_relajado;
    private $perimetro_muslo;
    private $perimetro_cintura;


    public function Evaluacion_nutricional()
    {
        $this->id="";
        $this->peso="";
        $this->talla="";
        $this->pliegue_tricipital="";
        $this->pliegue_bicipital="";
        $this->pliegue_abdominal="";
        $this->pliegue_muslo="";
        $this->perimetro_brazo_tenso="";
        $this->perimetro_brazo_relajado="";
        $this->perimetro_muslo="";
        $this->perimetro_cintura="";



    }

    public function setId($id)
    {  
        $this->id=$id;

    }

    public function getId()
    {
      return $this->id;

    }

    public function setPeso($peso)
    {
      $this->peso=$peso;
    }

    public function getPeso()
    {
      return $this->peso;
    }

    public function setTalla($talla)
    {
        $this->talla=$talla;
    }

    public function getTalla()
    {
        return $this->talla;
    }
	


     public function setPliegue_tricipital($pliegue_tricipital)
     {
          $this->pliegue_tricipital=$pliegue_tricipital;
     }

     public function getPliegue_tricipital()
     {
         return $this->pliegue_tricipital;
     }

  


    public function setPliegue_bicipital($pliegue_bicipital)
    {
      $this->pliegue_bicipital=$pliegue_bicipital;
    }

    public function getPliegue_bicipital()
    {
        return $this->pliegue_bicipital;
    }
    
    
    
    public function  setPliegue_abdominal($pliegue_abdominal)
    {
        $this->pliegue_abdominal=$pliegue_abdominal;
    }

    public function getPliegue_abdominal()
    {
        return $this->pliegue_abdominal;
    }


    public function  setPliegue_muslo($pliegue_muslo)
    {
        $this->$pliegue_muslo=$pliegue_muslo;
    }

    public function getPliegue_muslo()
    {
         return $this->pliegue_muslo;
    }
    
    
    
    public function setPerimetro_brazo_tenso($perimetro_brazo_tenso)
    {
            $this->perimetro_brazo_tenso=$perimetro_brazo_tenso;
    }

    public function getPerimetro_brazo_tenso()
    {
        return $this->perimetro_brazo_tenso;
    }
    
    
    public function setPerimetro_brazo_relajado($perimetro_brazo_relajado)
    {
       $this->perimetro_brazo_relajado=$perimetro_brazo_relajado;
    }

    public function getPerimetro_brazo_relajado()
    {
        return $this->perimetro_brazo_relajado;

    }
    
    public function setPerimetro_muslo($perimetro_muslo)
    {
       $this->perimetro_muslo=$perimetro_muslo;
    }

    public function getPerimetro_muslo()
    {
       return $this->perimetro_muslo;
    }
    
    public function setPerimetro_cintura($perimetro_cintura)
    {
       $this->perimetro_cintura=$perimetro_cintura;
    }
    
    public function getPerimetro_cintura()
    {
        return $this->perimetro_cintura;
    }
  








}


?>