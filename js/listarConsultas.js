//envío de formulario
$(document).ready(function () {
    $.ajax({
        type: "POST",
        url: "listarConsulta.php",
        data: {
            "estado": "Agendada"
        },
        success: (response) => {

            var jsonString = JSON.parse(response);
            jsonString.reverse();

            for (var clave in jsonString) {
                //console.log(jsonString[clave]);
                agregarConsulta(jsonString[clave]);
            }
        }
    })
});

$(document).ready(function () {

    $('#selectorEstados').click(function () {
        console.log('El texto seleccionado es:',
            $('select[id="selectorEstados"] option:selected').text());
    });
});

function iniciarConsulta(idConsultaAgendada) {

    document.cookie = "id-consulta="+idConsultaAgendada; 
    window.location.href = "./valoracion.html";
    


}

function cambiarEstado(idCosulta, estado) {
    console.log(idCosulta, estado);
}


/* <div align="right">
<label>Cambiar Estado: </label>
<select id="selectorEstados" class="btn btn btn-default dropdown-toggle ">
  <option value="Cancelada">Cancelar</option>
  <option value="Incumplida">Incumplida</option>
  <option value="Agendada" selected></option>
</select>
</div> */

function actualizarEstado(id) {
    var elementoS = document.getElementById(id);
    if (elementoS.value == "Cancelada") {
        alert("La consuta se ha cancelado ");
        let divBox = $(elementoS).parent().parent()[0];
        divBox.remove();

        $.ajax({
            type: "POST",
            url: "updateConsulta.php",
            data: {
                "idconsulta": id,
                "estado": "Cancelada"
            }
        })
    }
    else if (elementoS.value == "Incumplida") {
        alert("La consuta fue incumplida ");
        let divBox = $(elementoS).parent().parent()[0];
        divBox.remove();
    }

}



function agregarConsulta(elemento) {

    var item = document.createElement("li");
    item.setAttribute("class", "list-group-item ");
    document.getElementById("contact-list").appendChild(item);

    $(item).html(`<div align="right">
 <label>Cambiar Estado: </label>
 <select id="`+ elemento.id + `" class="btn btn btn-default dropdown-toggle " onclick="actualizarEstado(` + elemento.id + `);">
  <option value="Cancelada">Cancelar</option>
  <option value="Incumplida">Incumplida</option>
  <option value="Agendada" selected></option>
 </select>
 </div>`);


    var consulta = document.createElement("div");
    consulta.setAttribute("class", "row w-100 item");
    consulta.setAttribute("id", "item");

    item.appendChild(consulta);

    $(consulta).html(`<div  class="col-12 col-sm-6 col-md-3 px-0">
      <img src="imagengrisR.png" alt="Mike Anamendolla" class="rounded-circle mx-auto d-block img-fluid">
      </div> 
      <div id=target class="col-12 col-sm-3 col-md-6 text-center text-sm-left target">
        `);
    var nombre = document.createElement("label");
    nombre.setAttribute("class", "name lead");
    nombre.innerHTML = elemento.paciente_nombres + " " + elemento.paciente_apellidos;
    var it = document.getElementsByClassName("target");
    it[it.length - 1].appendChild(nombre);


    var espacio = document.createElement("br");
    var it = document.getElementsByClassName("target");
    it[it.length - 1].appendChild(espacio);

    var fecha = document.createElement("spam");
    fecha.setAttribute("class", "text-muted");
    fecha.innerHTML = elemento.fecha;
    fecha.setAttribute("id", "fecha");
    var it = document.getElementsByClassName("target");
    it[it.length - 1].appendChild(fecha);


    var espacio = document.createElement("br");
    var it = document.getElementsByClassName("target");
    it[it.length - 1].appendChild(espacio);




    var hora = document.createElement("spam");
    hora.setAttribute("class", "text-muted small text-truncate");
    hora.innerHTML = elemento.hora;
    var it = document.getElementsByClassName("target");
    it[it.length - 1].appendChild(hora);

    var espacio = document.createElement("br");
    var it = document.getElementsByClassName("target");
    it[it.length - 1].appendChild(espacio);

    var telefono = document.createElement("spam");
    telefono.setAttribute("class", "text-muted small");
    telefono.innerHTML = "Telefono: " + elemento.telefono;
    var it = document.getElementsByClassName("target");
    it[it.length - 1].appendChild(telefono);

    var espacio = document.createElement("br");
    var it = document.getElementsByClassName("target");
    it[it.length - 1].appendChild(espacio);

    var correo = document.createElement("spam");
    correo.setAttribute("class", "text-muted small text-truncate");
    correo.innerHTML = elemento.correo;
    var it = document.getElementsByClassName("target");
    it[it.length - 1].appendChild(correo);




    var boton_contenedor = document.createElement("div");
    boton_contenedor.setAttribute("class", "button-container");
    // boton_contenedor.setAttribute("cedula", elemento.paciente_documento)
    // boton_contenedor.setAttribute("idConsulta", elemento.id)
    // boton_contenedor.setAttribute("id", elemento.id)
    var it = document.getElementsByClassName("item");

    it[it.length - 1].appendChild(boton_contenedor);

    //console.log(it.length);
    //console.log(it);

    var button = document.createElement("button");
    button.setAttribute("class", "btn btn-success");
    button.innerHTML = "Iniciar consulta"
    button.setAttribute("type", "button");
    button.setAttribute("onclick", "iniciarConsulta('" + elemento.id + "')");
    boton_contenedor.appendChild(button);

    var buttonOpciones = document.createElement("button");
    buttonOpciones.setAttribute("class", "btn btn-success");
    buttonOpciones.innerHTML = "Iniciar consulta"
    buttonOpciones.setAttribute("type", "button");
    boton_contenedor.appendChild(button);

}


//Script para boton de mas opciones
jQuery(document).ready(function () {
    jQuery('.toggle-nav').click(function (e) {
        jQuery(this).toggleClass('active');
        jQuery('.menu ul').toggleClass('active');

        e.preventDefault();
    });
});



